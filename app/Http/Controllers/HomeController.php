<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\User;
use App\Message;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->Message=new Message();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //return view('home');
        $my_email=Auth::user()->email;
        //dd($my_email);
        $users_rs = User::where('email','<>',$my_email)->get();;
       // dd($users_rs);
        return view('index',compact('users_rs'));
    }
    
    public function get_msg($user_id=null,$no_of_message=null)
    {
        //dd($user_id,$no_of_message);
        //Sender Information
        $sender_id=Auth::user()->id;
        $sender_name=Auth::user()->name;			
		if(Auth::user()->user_image!=""){
			$sender_image=url('/').'/storage/app/'.Auth::user()->user_image;
		}else{
			$sender_image=url('/').'/storage/app/user_image/b.jpg';
		}
		
		//Receiver Information
        $receiver_id=$user_id;
        $receive_data=User::where('id',$receiver_id)->first();
        $receive_name=$receive_data->name;
		if($receive_data->user_image!=""){
			$receiver_image=url('/').'/storage/app/'.$receive_data->user_image;
		}else{
			$receiver_image=url('/').'/storage/app/user_image/c.jpg';
		}
		
        $no=0;
        $message = Message::where('status','<>','deleted')->where([
            ['sender_id', '=', $sender_id],
            ['receiver_id', '=', $receiver_id],
        ])->orWhere([
            ['sender_id', '=', $receiver_id],
            ['receiver_id', '=', $sender_id],
        ])//->orderBy('id', 'DESC')
        //->skip($no)->take(5)
        ->get();
       
        $data='';
        foreach($message as $msg)
        {
            
            if($msg->sender_id==$sender_id){
            $data.='<div class="d-flex justify-content-start mb-4">
                        <div class="img_cont_msg">
                            <img src="'.$sender_image.'" class="rounded-circle user_img_msg">
                            <span class="user-name">Me</span>
                        </div>
                        <div class="msg_cotainer">
                            '.$msg->message.'
                            <span class="msg_time">'.date("g:i A , d/m/Y",strtotime($msg->created_at)).'</span>
                        </div>
                    </div>';
            }else{
            $data.='<div class="d-flex justify-content-end mb-4">
								<div class="msg_cotainer_send">
                                    '.$msg->message.'
									<span class="msg_time_send">'.date("g:i A , d/m/Y",strtotime($msg->created_at)).'</span>
                                </div>
                                
								<div class="img_cont_msg">
                                    <img src="'.$receiver_image.'" class="rounded-circle user_img_msg">
                                    <span class="user-name">'.$receive_name.'</span>
								</div>
                            </div>';
            }                
        }
        return $data;
        
    }
    public function get_user($user_id=null)
    {
        $users_rs = User::where('id',$user_id)->first();
        $sender_id=Auth::user()->id;
        $receiver_id=$user_id;
		
		if($users_rs->user_image!=""){
			$receiver_image=url('/').'/storage/app/'.$users_rs->user_image;
		}else{
			$receiver_image=url('/').'/storage/app/user_image/c.jpg';
		}
		
        $message = Message::where('status','<>','deleted')->where([
            ['sender_id', '=', $sender_id],
            ['receiver_id', '=', $receiver_id],
        ])->orWhere([
            ['sender_id', '=', $receiver_id],
            ['receiver_id', '=', $sender_id],
        ])->orderBy('id', 'DESC')->get();
        $no_of_message = $message->count();

       // dd($no_of_message);
        $data='';
        //$data.='<input type="hidden" id="toprofile_image" val="'.$receiver_image.'">';
        $data.='
				<div class="img_cont">
					<img src="'.$receiver_image.'" class="rounded-circle user_img">
				</div>
				<div class="user_info">
                    <span>Chat with '.$users_rs->name.'</span><p>'.$no_of_message.' Messages</p>
                    <span id="receiver_id" style="display:none;">'.$users_rs->id.'</span>				
                    
				</div>
				';
        
        //return 'user';
        return $data;
    }
    public function sendMessage($receiver_id=null,$msg=null)
    {
       
        
        $sender_id=Auth::user()->id;
        $sender_name=Auth::user()->name;
        if(Auth::user()->user_image!=""){
			$sender_image=url('/').'/storage/app/'.Auth::user()->user_image;
		}else{
			$sender_image=url('/').'/storage/app/user_image/b.jpg';
        }
        
        $receive_data=User::where('id',$receiver_id)->first();
        $receive_name=$receive_data->name;
        if($receive_data->user_image!=""){
			$receiver_image=url('/').'/storage/app/'.$receive_data->user_image;
		}else{
			$receiver_image=url('/').'/storage/app/user_image/c.jpg';
        }
        

        $data['sender_id']=$sender_id;
        $data['receiver_id']=$receiver_id;
        $data['message']=$msg;
        $data['status']="unread";
        $insert  = $this->Message->create($data);
         $no=0;     
        $message = Message::where('status','<>','deleted')->where([
            ['sender_id', '=', $sender_id],
            ['receiver_id', '=', $receiver_id],
        ])->orWhere([
            ['sender_id', '=', $receiver_id],
            ['receiver_id', '=', $sender_id],
        ])
        //->orderBy('id', 'DESC')
        //->skip($no)->take(5)
        ->get();
        
        $data='';
        foreach($message as $msg)
        {
           
            if($msg->sender_id==$sender_id){
                $data.='<div class="d-flex justify-content-start mb-4">
                            <div class="img_cont_msg">
                                <img src="'.$sender_image.'" class="rounded-circle user_img_msg">
                                <span class="user-name">Me</span>
                            </div>
                            <div class="msg_cotainer">
                                '.$msg->message.'
                                <span class="msg_time">'.date("g:i A , d/m/Y",strtotime($msg->created_at)).'</span>
                            </div>
                        </div>';
                }else{
                $data.='<div class="d-flex justify-content-end mb-4">
                                    <div class="msg_cotainer_send">
                                        '.$msg->message.'
                                        <span class="msg_time_send">'.date("g:i A , d/m/Y",strtotime($msg->created_at)).'</span>
                                    </div>
                                    
                                    <div class="img_cont_msg">
                                        <img src="'.$receiver_image.'" class="rounded-circle user_img_msg">
                                        <span class="user-name">'.$receive_name.'</span>
                                    </div>
                                </div>';
                }   


            
        }
        return $data;
        
    }
    

    public function scroleMessage($user_id=null,$no_of_message=null)
    {
        // $now = Carbon::now();
        // $time=$now->format('g:i A');
        // $date=$now->format('d/m/Y');
        // dd($time,$date);
        // dd($user_id,$no_of_message);
       
        
        //dd($sender_image);
        
        $sender_id=Auth::user()->id;
        $sender_name=Auth::user()->name;
        if(Auth::user()->user_image!=""){
			$sender_image=url('/').'/storage/app/'.Auth::user()->user_image;
		}else{
			$sender_image=url('/').'/storage/app/user_image/b.jpg';
        }
       
        $receiver_id=$user_id;
        $receive_data=User::where('id',$receiver_id)->first();
        $receive_name=$receive_data->name;
        if($receive_data->user_image!=""){
			$receiver_image=url('/').'/storage/app/'.$receive_data->user_image;
		}else{
			$receiver_image=url('/').'/storage/app/user_image/c.jpg';
        }
        
        $no=$no_of_message;
       
        $message = Message::where('status','<>','deleted')->where([
            ['sender_id', '=', $sender_id],
            ['receiver_id', '=', $receiver_id],
        ])->orWhere([
            ['sender_id', '=', $receiver_id],
            ['receiver_id', '=', $sender_id],
        ])->orderBy('id', 'DESC')
        ->skip($no)->take(1)->get();
   
        $data='';
        foreach($message as $msg)
        {
           
            if($msg->sender_id==$sender_id){
            $data.='<div class="d-flex justify-content-start mb-4">
                        <div class="img_cont_msg">
                            
                            <img src="'.$sender_image.'" class="rounded-circle user_img_msg">
                            <span class="user-name">Me</span>
                        </div>
                        <div class="msg_cotainer">
                            '.$msg->message.'
                            <span class="msg_time">'.date("g:i A , d/m/Y",strtotime($msg->created_at)).'</span>
                        </div>
                    </div>';
            }else{
            $data.='<div class="d-flex justify-content-end mb-4">
								<div class="msg_cotainer_send">
                                    '.$msg->message.'
									<span class="msg_time_send">'.date("g:i A , d/m/Y",strtotime($msg->created_at)).'</span>
                                </div>
                                
                                <div class="img_cont_msg">
                                    
                                    <img src="'.$receiver_image.'" class="rounded-circle user_img_msg">
                                    <span class="user-name">'.$receive_name.'</span>
								</div>
                            </div>';
            }                
        }
       
        return $data;
        
    }

    public function searchByEmail($email=null){
        
        $user_id=Auth::user()->id;
        $user_data=User::where('email','like', $email.'%')->where('id','<>',$user_id)->get();
        //dd($user_data);
        $image_name=url('/').'/storage/app/user_image/D.jpg';
        $data='';
        if(isset($user_data)){
            foreach($user_data as $user){
            $data.='<li class="active">
                        <div class="d-flex bd-highlight" onclick="get_message_details('.$user->id.');">
                            <div class="img_cont">
                                <img src="'.$image_name.'" class="rounded-circle user_img">
                                
                            </div>
                            <div class="user_info">
                                <span>'.$user->name.'</span>
                                <p>'.$user->email.'</p>
                            </div>
                        </div>								
                    </li>';
            }
        }
        return $data;
    }
    public function searchByName($name=null){
        
        $user_id=Auth::user()->id;
        $user_data=User::where('name','like', $name.'%')->where('id','<>',$user_id)->get();
        //dd($user_data);
        $image_name=url('/').'/storage/app/user_image/D.jpg';
        $data='';
        if(isset($user_data)){
            foreach($user_data as $user){
            $data.='<li class="active">
                        <div class="d-flex bd-highlight" onclick="get_message_details('.$user->id.');">
                            <div class="img_cont">
                                <img src="'.$image_name.'" class="rounded-circle user_img">
                                
                            </div>
                            <div class="user_info">
                                <span>'.$user->name.'</span>
                                <p>'.$user->email.'</p>
                            </div>
                        </div>								
                    </li>';
            }
        }
        return $data;
    }

}

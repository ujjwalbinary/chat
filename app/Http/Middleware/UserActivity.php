<?php

namespace App\Http\Middleware;
use App\User;
use Carbon\Carbon;
use Auth;
use Closure;
use Cache;

class UserActivity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       if(Auth::check()){
           $expireAt=Carbon::now()->addMinutes(1);
           Cache::put('user-online-'.Auth::User()->id,true,$expireAt);
       }
        return $next($request);
    }
}

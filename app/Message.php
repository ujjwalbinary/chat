<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public $fillable = ['id', 'sender_id', 'receiver_id', 'message', 'status', 'created_at', 'updated_at'];
}

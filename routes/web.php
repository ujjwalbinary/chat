<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/get-msg-details/{user_id}/{no_of_message}', 'HomeController@get_msg');
Route::get('/get-user-details/{user_id}', 'HomeController@get_user');
Route::get('/send-message/{receiver_id}/{msg}', 'HomeController@sendMessage');
Route::get('/scroll-msg/{receiver_id}/{next_no}', 'HomeController@scroleMessage');
Route::get('/search-by-email/{email}', 'HomeController@searchByEmail');
Route::get('/search-by-name/{name}', 'HomeController@searchByName');

<!doctype html>
<html lang="en">


<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Document Title -->


@yield('head')

</head>

<body>

@yield('header')

@yield('content')

@yield('footer')

@yield('scripts')
  
</body>


</html>
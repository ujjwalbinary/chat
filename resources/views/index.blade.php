@extends('layouts.master')

@section('head')

<title>Chat</title>
	@include('partials.head')
@endsection

@section('header')
	@include('partials.header')
@endsection

@section('content')
<script src="http://localhost:3000/socket.io/socket.io.js"></script>	
<script>    
var socket =io.connect('http://localhost:3000');
</script>
  <div class="container-fluid h-100">
			<div class="row justify-content-center h-100">
				<div class="col-md-4 col-xl-3 chat"><div class="card mb-sm-3 mb-md-0 contacts_card">
					<div class="card-header">
					
					<div class="d-flex bd-highlight">
						<div class="img_cont">
							@if(Auth::user()->user_image!="")
								<img id="profile_pic" src="{{ url('/') }}/storage/app/{{Auth::user()->user_image}}" class="rounded-circle user_img">
							@else
								<img id="profile_pic" src="{{ url('/') }}/storage/app/user_image/D.jpg" class="rounded-circle user_img">
							@endif
							<!--<img src="{{ url('/') }}/storage/app/user_image/D.jpg" class="rounded-circle user_img">-->
							@if(Auth::user()->isOnline())
							<span class="online_icon"></span>
							@endif
						</div>
						<div class="user_info">
							<span id="fromusername">{{ Auth::user()->name }} </span>
							<p>{{ Auth::user()->email }} </p>
						</div>
					</div>
						<span id="action_menu1"><i class="fas fa-ellipsis-v"></i></span>
						<div class="action_menu1" style="display:none;">
							<ul>
								<li><a class="log-menu" href="{{ route('logout') }}"
									onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
									<i class="fas fa-user-circle"></i> Logout</a>
									</li>
									<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>		
							</ul>
						</div>
						<div class="input-group">
							<input type="text" placeholder="Search by name..." onkeyup="search_name(this.value);" class="form-control search mt-2">
							
							<!--<div class="input-group-prepend">
								<span class="input-group-text search_btn"><i class="fas fa-search"></i></span>
							</div>-->
							
						</div>
						<div class="input-group">
							<input type="text" placeholder="Search by email..." onkeyup="search_email(this.value);" class="form-control search mt-2">
						</div>
						
						
					</div>
					<div class="card-body contacts_body">
						<ui class="contacts" id="user_list">
						@if(isset($users_rs))
							@foreach($users_rs as $user)	
							<li class="active">
								<div class="d-flex bd-highlight" onclick="get_message_details({{$user->id}});">
									<div class="img_cont">
										@if($user->user_image!="")
											<img src="{{ url('/') }}/storage/app/{{$user->user_image}}" class="rounded-circle user_img">
										@else	
										<img src="{{ url('/') }}/storage/app/user_image/D.jpg" class="rounded-circle user_img">
										@endif
										@if($user->isOnline())
											<span class="online_icon"></span>
										@else
											<span class="online_icon offline"></span>
										@endif
									</div>
									<div class="user_info">
										<span>{{$user->name}}</span>
										<p>{{$user->email}}</p>
									</div>
								</div>								
							</li>
							@endforeach
						@endif
						</ui>
					</div>
					<div class="card-footer"></div>
				</div></div>
				<div class="col-md-8 col-xl-6 chat" id="chat_details" style="display:none">
					<div class="card">
						<div class="card-header msg_head">
							<div class="d-flex bd-highlight" id="user_info">
								<div class="img_cont">
									<img src="{{ url('/') }}/storage/app/user_image/D.jpg" class="rounded-circle user_img">
								</div>
								<div class="user_info">
									
								</div>
								
							</div>
							<!--<span id="action_menu_btn"><i class="fas fa-ellipsis-v"></i></span>
							<div class="action_menu">
								<ul>
									<li><i class="fas fa-user-circle"></i> View profile</li>
								</ul>
							</div>-->
						</div>
						<div class="card-body msg_card_body" id="msg_body">
							<!--<div class="d-flex justify-content-start mb-4">
								<div class="img_cont_msg">
									<img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" class="rounded-circle user_img_msg">
								</div>
								<div class="msg_cotainer">
									Hi, how are you samim?
									<span class="msg_time">8:40 AM, Today</span>
								</div>
							</div>
							<div class="d-flex justify-content-end mb-4">
								<div class="msg_cotainer_send">
									Hi Khalid i am good tnx how about you?
									<span class="msg_time_send">8:55 AM, Today</span>
								</div>
								<div class="img_cont_msg">
							<img src="" class="rounded-circle user_img_msg">
								</div>
							</div>
							<div class="d-flex justify-content-start mb-4">
								<div class="img_cont_msg">
									<img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" class="rounded-circle user_img_msg">
								</div>
								<div class="msg_cotainer">
									I am good too, thank you for your chat template
									<span class="msg_time">9:00 AM, Today</span>
								</div>
							</div>
							<div class="d-flex justify-content-end mb-4">
								<div class="msg_cotainer_send">
									You are welcome
									<span class="msg_time_send">9:05 AM, Today</span>
								</div>
								<div class="img_cont_msg">
							<img src="" class="rounded-circle user_img_msg">
								</div>
							</div>
							<div class="d-flex justify-content-start mb-4">
								<div class="img_cont_msg">
									<img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" class="rounded-circle user_img_msg">
								</div>
								<div class="msg_cotainer">
									I am looking for your next templates
									<span class="msg_time">9:07 AM, Today</span>
								</div>
							</div>
							<div class="d-flex justify-content-end mb-4">
								<div class="msg_cotainer_send">
									Ok, thank you have a good day
									<span class="msg_time_send">9:10 AM, Today</span>
								</div>
								<div class="img_cont_msg">
									<img src="" class="rounded-circle user_img_msg">
								</div>
							</div>
							<div class="d-flex justify-content-start mb-4">
								<div class="img_cont_msg">
									<img src="https://static.turbosquid.com/Preview/001292/481/WV/_D.jpg" class="rounded-circle user_img_msg">
								</div>
								<div class="msg_cotainer">
									Bye, see you
									<span class="msg_time">9:12 AM, Today</span>
								</div>
							</div>-->
						</div>
						<div class="card-footer">
							
							<div class="input-group">
								<!--<div class="input-group-append">
									<span class="input-group-text attach_btn"><i class="fas fa-paperclip"></i></span>
								</div>-->
								<textarea name="msg" id="msg" class="form-control type_msg" placeholder="Type your message..."></textarea>
								<div class="input-group-append">
									<span class="input-group-text send_btn" id="send"><i class="fas fa-location-arrow"></i></span>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<input type="hidden" value="5" id="next_no">
@endsection  

@section('footer')
	@include('partials.footer')
@endsection
  
@section('scripts')
	@include('partials.scripts')
	
	
	
	<script>
	$(document).ready(function(){
		var userid=<?php echo Auth::user()->id; ?>;
		//alert(userid);
		//socket.emit('add user', userid);
		$("#msg_body").scroll(function(){			
			let div = $(this).get(0);
				if(div.scrollTop + div.clientHeight >= div.scrollHeight) {
					//appendData();
				}
		});
		
		socket.emit('join',{
				userid:userid				
			});
		socket.on('new message', (data) => {
			var open_user=$("#receiver_id").html();	
			var d = new Date();	
			var hours = d.getHours();
  			var minutes = d.getMinutes();
			var ampm = hours >= 12 ? 'PM' : 'AM';
			hours = hours % 12;
			hours = hours ? hours : 12; // the hour '0' should be '12'
			minutes = minutes < 10 ? '0'+minutes : minutes;
			var strTime = hours + ':' + minutes + ' ' + ampm + ', '+d. getDate()+'/'+(d. getMonth()+1)+'/'+d. getFullYear();		
			console.log(open_user);
			console.log(data);
			
			received_from=data.fromuserid;
			received_from=received_from.toString();
			if(open_user===received_from){
				console.log('received');
				var receive_msg='<div class="d-flex justify-content-end mb-4"><div class="msg_cotainer_send">'+data.message+'<span class="msg_time_send">'+strTime+'</span></div><div class="img_cont_msg"><img src="'+data.profile_pic+'" class="rounded-circle user_img_msg"><span class="user-name">'+data.fromusername+'</span></div></div>';
				$("#msg_body").append(receive_msg);
			}
		});
	});
	function get_message_details(user_id){
		$("#chat_details").show();
		var no_of_message=1;
		$.ajax({          
			url:'{{url('get-msg-details')}}/'+user_id+'/'+no_of_message,
			type: "GET",
			success: function(jsonStr) {          
			  
			  $("#msg_body").html(jsonStr);
			  $('#msg_body').scrollTop($('#msg_body')[0].scrollHeight);
			  $.ajax({          
				url:'{{url('get-user-details')}}/'+user_id,
				type: "GET",
					success: function(jsonStr) { 
					$("#user_info").html(jsonStr);					
					}
				});	
			}
			});
		
			
	}
	</script>
	<script>
		
	$( "#send" ).click(function() {
		var msg=$("#msg").val();
		var receiver_id=$("#receiver_id").html();
		var fromusername=$("#fromusername").html();
		var profile_pic=$("#profile_pic").prop('src');
		//var profile_pic=
		//alert(profile_pic);
		if(msg!==''){
		$.ajax({          
			url:'{{url('send-message')}}/'+receiver_id+'/'+msg,
			type: "GET",
			success: function(jsonStr) {          
			  
			  $("#msg_body").html(jsonStr);
			  $('#msg_body').scrollTop($('#msg_body')[0].scrollHeight);			  
			  $("#msg").val('');
			  $.ajax({          
				url:'{{url('get-user-details')}}/'+receiver_id,
				type: "GET",
					success: function(jsonStr) { 						
					$("#user_info").html(jsonStr);					
					}
				});			  
			}
			});
			var userid=<?php echo Auth::user()->id; ?>;
			//socket.emit('add user', receiver_id);
			socket.emit('new message',{
				fromuserid:userid,
				fromusername:fromusername,
				msg:msg,
				touser:receiver_id,
				profile_pic:profile_pic
			});
		}	
			
	});
	</script>
	
<script type="text/javascript">
	
	
	function appendData() {
		
		var next_no = $("#next_no").val();
		var receiver_id=$("#receiver_id").html();
		var sum = 0;
		
        //alert(receiver_id);
       // alert(next_no);
		var html = '';
		$.ajax({          
			url:'{{url('/scroll-msg/')}}/'+receiver_id+'/'+next_no,
			type: "GET",
			success: function(response) {  
				html +=response;  
				//alert(response);
			  $('#msg_body').append(html);
			  var sum = parseInt(next_no)+1;
			 // alert(sum);
			  $("#next_no").val(sum);
			}
		});
	}
</script>
<script>
function search_email(email){
	//alert(email);
	if(email!==''){
	$.ajax({          
			url:'{{url('/search-by-email/')}}/'+email,
			type: "GET",
			success: function(response) { 
			  $('#user_list').html(response);			 
			}
		});
	}else{
		window.location.reload();
	}	
}
</script>
<script>
function search_name(name){
	//alert(email);
	if(name!==''){
	$.ajax({          
			url:'{{url('/search-by-name/')}}/'+name,
			type: "GET",
			success: function(response) { 
			  $('#user_list').html(response);			 
			}
		});
	}else{
		window.location.reload();
	}	
}
</script>
@endsection



